//
//  CoinData.swift
//  ByteCoin
//
//  Created by Lorin Sherry on 23/02/2022.
//  Copyright © 2022 The App Brewery. All rights reserved.
//

import Foundation

struct CoinData: Decodable {
    let rate: Double 
}
